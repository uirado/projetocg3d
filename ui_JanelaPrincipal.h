/********************************************************************************
** Form generated from reading UI file 'JanelaPrincipal.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JANELAPRINCIPAL_H
#define UI_JANELAPRINCIPAL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QPushButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "MeuPainelOpenGL.h"

QT_BEGIN_NAMESPACE

class Ui_JanelaPrincipal
{
public:
    QVBoxLayout *verticalLayout;
    MeuPainelOpenGL *painelGL;
    QHBoxLayout *horizontalLayout;
    QGroupBox *groupBox_3;
    QLabel *labelJ1;
    QGroupBox *groupBox_2;
    QPushButton *bt_sortear;
    QLabel *labelDado1;
    QPushButton *bt_dado;
    QLabel *labelDado2;
    QPushButton *bt_reiniciar;
    QGroupBox *groupBox;
    QLabel *labelJ2;

    void setupUi(QWidget *JanelaPrincipal)
    {
        if (JanelaPrincipal->objectName().isEmpty())
            JanelaPrincipal->setObjectName(QString::fromUtf8("JanelaPrincipal"));
        JanelaPrincipal->setEnabled(true);
        JanelaPrincipal->resize(1024, 768);
        verticalLayout = new QVBoxLayout(JanelaPrincipal);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        painelGL = new MeuPainelOpenGL(JanelaPrincipal);
        painelGL->setObjectName(QString::fromUtf8("painelGL"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(painelGL->sizePolicy().hasHeightForWidth());
        painelGL->setSizePolicy(sizePolicy);
        painelGL->setSizeIncrement(QSize(0, 0));
        painelGL->setBaseSize(QSize(0, 0));
        painelGL->setMouseTracking(false);
        painelGL->setLayoutDirection(Qt::RightToLeft);
        painelGL->setAutoFillBackground(false);

        verticalLayout->addWidget(painelGL);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        groupBox_3 = new QGroupBox(JanelaPrincipal);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        groupBox_3->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        groupBox_3->setAlignment(Qt::AlignCenter);
        groupBox_3->setFlat(true);
        labelJ1 = new QLabel(groupBox_3);
        labelJ1->setObjectName(QString::fromUtf8("labelJ1"));
        labelJ1->setGeometry(QRect(0, 40, 421, 31));
        QFont font;
        font.setPointSize(20);
        font.setBold(false);
        font.setWeight(50);
        labelJ1->setFont(font);
        labelJ1->setStyleSheet(QString::fromUtf8("color: rgb(85, 255, 255);"));
        labelJ1->setTextFormat(Qt::AutoText);
        labelJ1->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(groupBox_3);

        groupBox_2 = new QGroupBox(JanelaPrincipal);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        groupBox_2->setAlignment(Qt::AlignCenter);
        bt_sortear = new QPushButton(groupBox_2);
        bt_sortear->setObjectName(QString::fromUtf8("bt_sortear"));
        bt_sortear->setGeometry(QRect(20, 20, 101, 41));
        labelDado1 = new QLabel(groupBox_2);
        labelDado1->setObjectName(QString::fromUtf8("labelDado1"));
        labelDado1->setGeometry(QRect(0, 70, 31, 31));
        QFont font1;
        font1.setPointSize(16);
        font1.setBold(true);
        font1.setWeight(75);
        labelDado1->setFont(font1);
        labelDado1->setAlignment(Qt::AlignCenter);
        bt_dado = new QPushButton(groupBox_2);
        bt_dado->setObjectName(QString::fromUtf8("bt_dado"));
        bt_dado->setGeometry(QRect(20, 20, 101, 41));
        bt_dado->setCheckable(false);
        bt_dado->setChecked(false);
        labelDado2 = new QLabel(groupBox_2);
        labelDado2->setObjectName(QString::fromUtf8("labelDado2"));
        labelDado2->setGeometry(QRect(110, 70, 31, 31));
        labelDado2->setFont(font1);
        labelDado2->setAlignment(Qt::AlignCenter);
        bt_reiniciar = new QPushButton(groupBox_2);
        bt_reiniciar->setObjectName(QString::fromUtf8("bt_reiniciar"));
        bt_reiniciar->setGeometry(QRect(20, 20, 101, 41));
        bt_reiniciar->raise();
        bt_dado->raise();
        bt_sortear->raise();
        labelDado1->raise();
        labelDado2->raise();

        horizontalLayout->addWidget(groupBox_2);

        groupBox = new QGroupBox(JanelaPrincipal);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        groupBox->setStyleSheet(QString::fromUtf8("background-color: rgb(0, 0, 0);"));
        groupBox->setAlignment(Qt::AlignCenter);
        groupBox->setFlat(true);
        labelJ2 = new QLabel(groupBox);
        labelJ2->setObjectName(QString::fromUtf8("labelJ2"));
        labelJ2->setGeometry(QRect(10, 40, 411, 31));
        labelJ2->setFont(font);
        labelJ2->setStyleSheet(QString::fromUtf8("color: rgb(255, 255, 0);"));
        labelJ2->setTextFormat(Qt::AutoText);
        labelJ2->setAlignment(Qt::AlignCenter);

        horizontalLayout->addWidget(groupBox);

        horizontalLayout->setStretch(0, 3);
        horizontalLayout->setStretch(1, 1);
        horizontalLayout->setStretch(2, 3);

        verticalLayout->addLayout(horizontalLayout);

        verticalLayout->setStretch(0, 6);
        verticalLayout->setStretch(1, 1);

        retranslateUi(JanelaPrincipal);
        QObject::connect(bt_sortear, SIGNAL(clicked()), painelGL, SLOT(sortear()));
        QObject::connect(painelGL, SIGNAL(setJogador1(QString)), labelJ1, SLOT(setText(QString)));
        QObject::connect(painelGL, SIGNAL(setJogador2(QString)), labelJ2, SLOT(setText(QString)));
        QObject::connect(painelGL, SIGNAL(setValorDado1(int)), labelDado1, SLOT(setNum(int)));
        QObject::connect(painelGL, SIGNAL(hideBotaoSortear()), bt_sortear, SLOT(hide()));
        QObject::connect(painelGL, SIGNAL(hideBotaoDado()), bt_dado, SLOT(hide()));
        QObject::connect(bt_dado, SIGNAL(clicked()), painelGL, SLOT(jogarDado()));
        QObject::connect(painelGL, SIGNAL(setValorDado2(int)), labelDado2, SLOT(setNum(int)));
        QObject::connect(painelGL, SIGNAL(showBotaoDado()), bt_dado, SLOT(show()));
        QObject::connect(painelGL, SIGNAL(setDadoVazio1(QString)), labelDado1, SLOT(setText(QString)));
        QObject::connect(painelGL, SIGNAL(setDadoVazio2(QString)), labelDado2, SLOT(setText(QString)));
        QObject::connect(painelGL, SIGNAL(showBotaoSortear()), bt_sortear, SLOT(show()));
        QObject::connect(bt_reiniciar, SIGNAL(clicked()), painelGL, SLOT(reiniciar()));
        QObject::connect(painelGL, SIGNAL(hideBotaoReiniciar()), bt_reiniciar, SLOT(hide()));
        QObject::connect(painelGL, SIGNAL(showBotaoReiniciar()), bt_reiniciar, SLOT(show()));

        QMetaObject::connectSlotsByName(JanelaPrincipal);
    } // setupUi

    void retranslateUi(QWidget *JanelaPrincipal)
    {
        JanelaPrincipal->setWindowTitle(QApplication::translate("JanelaPrincipal", "Projeto OpenGL", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QString());
        labelJ1->setText(QString());
        groupBox_2->setTitle(QString());
        bt_sortear->setText(QApplication::translate("JanelaPrincipal", "Sortear in\303\255cio", 0, QApplication::UnicodeUTF8));
        labelDado1->setText(QString());
        bt_dado->setText(QApplication::translate("JanelaPrincipal", "Jogar dado", 0, QApplication::UnicodeUTF8));
        labelDado2->setText(QString());
        bt_reiniciar->setText(QApplication::translate("JanelaPrincipal", "Reiniciar", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QString());
        labelJ2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class JanelaPrincipal: public Ui_JanelaPrincipal {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JANELAPRINCIPAL_H
