# -------------------------------------------------
# Project created by QtCreator 2010-03-14T11:09:30
# -------------------------------------------------
QT += opengl
TARGET = ProjetoOpenGL
TEMPLATE = app
SOURCES += main.cpp \
    JanelaPrincipal.cpp \
    MeuPainelOpenGL.cpp \
    Camera.cpp \
    Tabuleiro.cpp \
    Util.cpp \
    Pino.cpp
HEADERS += JanelaPrincipal.h \
    MeuPainelOpenGL.h \
    Util.h \
    Camera.h \
    Tabuleiro.h \
    Util.h \
    Util.h \
    Pino.h
FORMS += JanelaPrincipal.ui

DISTFILES +=
