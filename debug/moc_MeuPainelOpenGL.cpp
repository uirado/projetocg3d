/****************************************************************************
** Meta object code from reading C++ file 'MeuPainelOpenGL.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../MeuPainelOpenGL.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MeuPainelOpenGL.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MeuPainelOpenGL[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      15,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
      12,       // signalCount

 // signals: signature, parameters, type, tag, flags
      21,   17,   16,   16, 0x05,
      42,   17,   16,   16, 0x05,
      69,   63,   16,   16, 0x05,
      88,   63,   16,   16, 0x05,
     107,   17,   16,   16, 0x05,
     130,   17,   16,   16, 0x05,
     153,   16,   16,   16, 0x05,
     172,   16,   16,   16, 0x05,
     188,   16,   16,   16, 0x05,
     204,   16,   16,   16, 0x05,
     223,   16,   16,   16, 0x05,
     244,   16,   16,   16, 0x05,

 // slots: signature, parameters, type, tag, flags
     265,   16,   16,   16, 0x0a,
     275,   16,   16,   16, 0x0a,
     287,   16,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MeuPainelOpenGL[] = {
    "MeuPainelOpenGL\0\0str\0setJogador1(QString)\0"
    "setJogador2(QString)\0valor\0"
    "setValorDado1(int)\0setValorDado2(int)\0"
    "setDadoVazio1(QString)\0setDadoVazio2(QString)\0"
    "hideBotaoSortear()\0hideBotaoDado()\0"
    "showBotaoDado()\0showBotaoSortear()\0"
    "hideBotaoReiniciar()\0showBotaoReiniciar()\0"
    "sortear()\0jogarDado()\0reiniciar()\0"
};

void MeuPainelOpenGL::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MeuPainelOpenGL *_t = static_cast<MeuPainelOpenGL *>(_o);
        switch (_id) {
        case 0: _t->setJogador1((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 1: _t->setJogador2((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->setValorDado1((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->setValorDado2((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->setDadoVazio1((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 5: _t->setDadoVazio2((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 6: _t->hideBotaoSortear(); break;
        case 7: _t->hideBotaoDado(); break;
        case 8: _t->showBotaoDado(); break;
        case 9: _t->showBotaoSortear(); break;
        case 10: _t->hideBotaoReiniciar(); break;
        case 11: _t->showBotaoReiniciar(); break;
        case 12: _t->sortear(); break;
        case 13: _t->jogarDado(); break;
        case 14: _t->reiniciar(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MeuPainelOpenGL::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MeuPainelOpenGL::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_MeuPainelOpenGL,
      qt_meta_data_MeuPainelOpenGL, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MeuPainelOpenGL::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MeuPainelOpenGL::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MeuPainelOpenGL::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MeuPainelOpenGL))
        return static_cast<void*>(const_cast< MeuPainelOpenGL*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int MeuPainelOpenGL::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 15)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 15;
    }
    return _id;
}

// SIGNAL 0
void MeuPainelOpenGL::setJogador1(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void MeuPainelOpenGL::setJogador2(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void MeuPainelOpenGL::setValorDado1(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}

// SIGNAL 3
void MeuPainelOpenGL::setValorDado2(int _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 3, _a);
}

// SIGNAL 4
void MeuPainelOpenGL::setDadoVazio1(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 4, _a);
}

// SIGNAL 5
void MeuPainelOpenGL::setDadoVazio2(QString _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 5, _a);
}

// SIGNAL 6
void MeuPainelOpenGL::hideBotaoSortear()
{
    QMetaObject::activate(this, &staticMetaObject, 6, 0);
}

// SIGNAL 7
void MeuPainelOpenGL::hideBotaoDado()
{
    QMetaObject::activate(this, &staticMetaObject, 7, 0);
}

// SIGNAL 8
void MeuPainelOpenGL::showBotaoDado()
{
    QMetaObject::activate(this, &staticMetaObject, 8, 0);
}

// SIGNAL 9
void MeuPainelOpenGL::showBotaoSortear()
{
    QMetaObject::activate(this, &staticMetaObject, 9, 0);
}

// SIGNAL 10
void MeuPainelOpenGL::hideBotaoReiniciar()
{
    QMetaObject::activate(this, &staticMetaObject, 10, 0);
}

// SIGNAL 11
void MeuPainelOpenGL::showBotaoReiniciar()
{
    QMetaObject::activate(this, &staticMetaObject, 11, 0);
}
QT_END_MOC_NAMESPACE
