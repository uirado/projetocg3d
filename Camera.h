#ifndef CAMERA_H
#define CAMERA_H

#include <GL/glu.h>


class Camera
{
public:
    Camera();

    float getX() const;
    void setX(float value);

    float getY() const;
    void setY(float value);

    float getZ() const;
    void setZ(float value);

    float getFoco_x() const;
    void setFoco_x(float value);

    float getFoco_y() const;
    void setFoco_y(float value);

    float getFoco_z() const;
    void setFoco_z(float value);

    void setAll(float x, float y, float z, float foco_x, float foco_y, float foco_z);

    void rotateX(float a);
    void rotateY(float a);
    void rotateZ(float a);
    void rotate(float x, float y, float z);

    void moveForward(float distance);
    void moveBackward(float distance);
    void moveLeft(float distance);
    void moveRight(float distance);
    void reset();

    void desenhaFoco();

    GLfloat matriz[16];

private:
    float x;
    float y;
    float z;
    float foco_x;
    float foco_y;
    float foco_z;

};

#endif // CAMERA_H
