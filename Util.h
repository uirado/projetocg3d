#ifndef UTIL_H
#define UTIL_H
#include <GL/gl.h>

class Util
{
public:
    Util();
    static GLuint LoadBMP(const char *fileName);
};

#endif // UTIL_H
