#include "Tabuleiro.h"
#include <GL/glu.h>
#include <math.h>
#include "Util.h"
#include <stdio.h>
#include <stdlib.h>
#include <QDebug>


Tabuleiro::Tabuleiro()
{

}

void Tabuleiro::createMatriz()
{
    int i,j;
    matriz = (int**) malloc(8*sizeof(int*));

    for(i = 0; i < 8; i++){
        matriz[i] = (int*) malloc (8*sizeof(int));
    }
    for(i = 0; i < 8; i++){
        for (j = 0; j < 8; j ++){
            matriz[i][j] = 0;
        }
    }
    matriz[0][2] = -1;
    matriz[1][2] = 10;
    matriz[2][2] = 1;
    matriz[2][1] = 1;
    matriz[3][1] = 1;
    matriz[4][1] = 3;
    matriz[4][2] = 1;
    matriz[4][3] = 1;
    matriz[4][4] = 3;
    matriz[3][4] = 1;
    matriz[2][4] = 1;
    matriz[1][4] = 1;
    matriz[1][5] = 4;
    matriz[1][6] = 1;
    matriz[2][6] = 1;
    matriz[3][6] = 1;
    matriz[4][6] = 6;
    matriz[5][6] = 1;
    matriz[6][6] = 1;
    matriz[6][5] = 5;
    matriz[6][4] = 4;
    matriz[6][3] = 1;
    matriz[7][3] = -2;

//    matriz[8][8] = {
//       {0, 0,-2, 0, 0, 0, 0, 0},
//       {0, 0,10, 0, 1, 4, 1, 0},
//       {0, 1, 1, 0, 1, 0, 1, 0},
//       {0, 1, 0, 0, 1, 0, 1, 0},
//       {0, 3, 1, 1, 3, 0, 6, 0},
//       {0, 0, 0, 0, 0, 0, 1, 0},
//       {0, 0, 0, 1, 4, 5, 1, 0},
//       {0, 0, 0,-1, 0, 0, 0, 0}
//    };
}


int **Tabuleiro::getMatriz()
{
    return matriz;
}

void Tabuleiro::desenha()
{
    int i,j;
    float x,y,z;
    //normais
    int nXZ1[] = {0,-1,0};
    int nXZ2[] = {0,1,0};
    int nYZ1[] = {-1,0,0};
    int nYZ2[] = {1,0,0};
    int nXY1[] = {0,0,-1};
    int nXY2[] = {0,0,1};

    x = 0;
    y = 0;
    z = 0;



    for(i = 0; i < 8; i++){
        z = i;
        for(j = 0; j < 8; j++){
            x = j;
            escolheCor(matriz[i][j]);
            desenhaFaceXZ(x, y, z, tamanhoCasa, offset, nXZ1);
            escolheCor(matriz[i][j]);
            desenhaFaceXZ(x, tamanhoCasa-offset, z, tamanhoCasa, offset, nXZ2);

            escolheCor(matriz[i][j]);
            desenhaFaceYZ(x*tamanhoCasa, y, z, tamanhoCasa, offset, nYZ1);
            escolheCor(matriz[i][j]);
            desenhaFaceYZ(x*tamanhoCasa+tamanhoCasa-offset, y, z, tamanhoCasa, offset, nYZ2);

            escolheCor(matriz[i][j]);
            desenhaFaceXY(x, y, z*tamanhoCasa, tamanhoCasa, offset, nXY1);
            escolheCor(matriz[i][j]);
            desenhaFaceXY(x, y, z*tamanhoCasa+tamanhoCasa-offset, tamanhoCasa, offset, nXY2);
        }
    }
    glFlush();
}

float Tabuleiro::getTamanhoCasa() const
{
    return tamanhoCasa;
}

float Tabuleiro::getOffset() const
{
    return offset;
}

void Tabuleiro::setOffset(float value)
{
    offset = value;
}

void Tabuleiro::setTamanhoCasa(float value)
{
    tamanhoCasa = value;
}

void Tabuleiro::desenhaFaceXZ(float x, float y, float z, float tamanho, float offset, int n[]){
    glBegin(GL_QUADS);
        glNormal3f(n[0],n[1],n[2]);
        glVertex3f(x*tamanho, y, z*tamanho);
        glVertex3f(x*tamanho + tamanho -offset, y, z*tamanho);
        glVertex3f(x*tamanho + tamanho -offset, y, z*tamanho + tamanho -offset);
        glVertex3f(x*tamanho, y, z*tamanho + tamanho -offset);
    glEnd();
    glDisable(GL_TEXTURE_2D);
//    glLineWidth(3);
//    glColor3f(.2,.2,.2);
//    glBegin(GL_LINE_LOOP);
//        glNormal3f(n[0],n[1],n[2]);
//        glVertex3f(x*tamanho, y, z*tamanho);
//        glVertex3f(x*tamanho + tamanho-offset, y, z*tamanho);
//        glVertex3f(x*tamanho + tamanho-offset, y, z*tamanho + tamanho-offset);
//        glVertex3f(x*tamanho, y, z*tamanho + tamanho-offset);
//    glEnd();
}

void Tabuleiro::desenhaFaceYZ(float x, float y, float z, float tamanho, float offset, int n[]){

    glBegin(GL_QUADS);
        glNormal3f(n[0],n[1],n[2]);
        glVertex3f(x, y*tamanho, z*tamanho);
        glVertex3f(x, y*tamanho + tamanho -offset, z*tamanho);
        glVertex3f(x, y*tamanho + tamanho -offset, z*tamanho + tamanho -offset);
        glVertex3f(x, y*tamanho, z*tamanho + tamanho -offset);
    glEnd();

//    glLineWidth(3);
//    glColor3f(.2,.2,.2);
//    glBegin(GL_LINE_LOOP);
//    glNormal3f(-1,0,0);
//        glNormal3f(n[0],n[1],n[2]);
//        glVertex3f(x, y*tamanho, z*tamanho);
//        glVertex3f(x, y*tamanho + tamanho -offset, z*tamanho);
//        glVertex3f(x, y*tamanho + tamanho -offset, z*tamanho + tamanho -offset);
//        glVertex3f(x, y*tamanho, z*tamanho + tamanho -offset);
//    glEnd();
}

void Tabuleiro::desenhaFaceXY(float x, float y, float z, float tamanho, float offset, int n[]){
    glBegin(GL_QUADS);
        glNormal3f(n[0],n[1],n[2]);
        glVertex3f(x*tamanho, y*tamanho, z);
        glVertex3f(x*tamanho+tamanho-offset, y*tamanho, z);
        glVertex3f(x*tamanho+tamanho-offset, y*tamanho + tamanho -offset, z);
        glVertex3f(x*tamanho, y*tamanho+tamanho-offset, z);
    glEnd();

//    glLineWidth(3);
//    glColor3f(.2,.2,.2);
//    glBegin(GL_LINE_LOOP);
//        glNormal3f(n[0],n[1],n[2]);
//        glVertex3f(x*tamanho, y*tamanho, z);
//        glVertex3f(x*tamanho+tamanho-offset, y*tamanho, z);
//        glVertex3f(x*tamanho+tamanho-offset, y*tamanho + tamanho -offset, z);
//        glVertex3f(x*tamanho, y*tamanho+tamanho-offset, z);
//    glEnd();
}

void Tabuleiro::escolheCor(int valor){
    if(valor == 0){ //vazio cinza
        glColor3f(.8f,.8f,.8f);
    } else if(valor == 1){ //caminho normal azul
        glColor3f(.0f,0.5f,1.0f);
    } else if(valor == -1){ //inicio quase preto
        glColor3f(.1,.1,.1);
    } else if(valor == -2){ //chegada verde
        glColor3f(0.2f,1,0.0f);
    } else{ //armadilha vermelho
        glColor3f(1, 0.2f, 0.0f);
    }
}
