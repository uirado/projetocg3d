#ifndef TABULEIRO_H
#define TABULEIRO_H

#include <vector>


class Tabuleiro
{
public:
    Tabuleiro();



    void desenha();

    float getTamanhoCasa() const;

    float getOffset() const;
    void setOffset(float value);

    void setTamanhoCasa(float value);

    int **getMatriz();

    void createMatriz();

private:
    //std::vector < std::vector<int> > matriz;
    float tamanhoCasa, offset;
    int **matriz;

    void desenhaFaceXZ(float x, float y, float z, float tamanho, float offset, int n[]);
    void desenhaFaceYZ(float x, float y, float z, float tamanho, float offset, int n[]);
    void desenhaFaceXY(float x, float y, float z, float tamanho, float offset, int n[]);
    void escolheCor(int valor);
};

#endif // TABULEIRO_H
