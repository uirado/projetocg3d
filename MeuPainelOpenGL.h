#ifndef MEUPAINELOPENGL_H
#define MEUPAINELOPENGL_H

#include "Camera.h"
#include "Pino.h"
#include "Tabuleiro.h"

#include <GL/glu.h>
#include <QGLWidget>

class MeuPainelOpenGL : public QGLWidget {

Q_OBJECT
public:
    explicit MeuPainelOpenGL(QWidget *parent = 0);

signals:
    void setJogador1(QString str);
    void setJogador2(QString str);
    void setValorDado1(int valor);
    void setValorDado2(int valor);
    void setDadoVazio1(QString str);
    void setDadoVazio2(QString str);
    void hideBotaoSortear();
    void hideBotaoDado();
    void showBotaoDado();
    void showBotaoSortear();
    void hideBotaoReiniciar();
    void showBotaoReiniciar();

public slots:
    void sortear();
    void jogarDado();
    void reiniciar();

protected:
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void wheelEvent(QWheelEvent *event);

private:
    QPoint lastPoint;
    GLfloat windowHeight;
    GLfloat windowWidth;

    Tabuleiro tabuleiro;
    int jogadorAtual;

    void trocarJogador();

    Pino pino1;
    Pino pino2;

    Camera cam;

    int xRot;
    int yRot;
    int zRot;
    int xT;
    int yT;
    int zT;
    float zoom;
    int malhaSize;

    void setupCamera();
    void setXRotation(int angle);
    void setYRotation(int angle);
    void setZRotation(int angle);
    void resetView();
    void setZoom(float valor);
    float getZoom();

    void updateMalha(int valor);

    void desenhaEixos();
    void desenhaMalhaChao();

    void configurarLuz();
};

#endif // MEUPAINELOPENGL_H
