#include "Pino.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <QDebug>

Pino::Pino()
{

}

void Pino::draw()
{
    glColor3f(r, g, b);
    //desenha base inferior
    glBegin(GL_QUADS);
        glNormal3f(0,-1,0);
        glVertex3f(x, y, z);
        glVertex3f(x+baseSize, y, z);
        glVertex3f(x+baseSize, y, z+baseSize);
        glVertex3f(x, y, z+baseSize);
    glEnd();

    //desenha base superior
    glBegin(GL_QUADS);
        glNormal3f(0,1,0);
        glVertex3f(x, y+height, z);
        glVertex3f(x+baseSize, y+height, z);
        glVertex3f(x+baseSize, y+height, z+baseSize);
        glVertex3f(x, y+height, z+baseSize);
    glEnd();

    //desenha faces xy z=0
    glBegin(GL_QUADS);
        glNormal3i(0,0,-1);
        glVertex3f(x, y, z);
        glVertex3f(x+baseSize, y, z);
        glVertex3f(x+baseSize, y+height, z);
        glVertex3f(x, y+height, z);
    glEnd();

    //desenha faces xy z=baseSize
    glBegin(GL_QUADS);
        glNormal3f(0,0,1);
        glVertex3f(x, y, z+baseSize);
        glVertex3f(x+baseSize, y, z+baseSize);
        glVertex3f(x+baseSize, y+height, z+baseSize);
        glVertex3f(x, y+height, z+baseSize);
    glEnd();

    //desenha faces zy x=0
    glBegin(GL_QUADS);
        glNormal3f(-1,0,0);
        glVertex3f(x, y, z);
        glVertex3f(x, y, z+baseSize);
        glVertex3f(x, y+height, z+baseSize);
        glVertex3f(x, y+height, z);
    glEnd();

    //desenha faces zy x=baseSize
    glBegin(GL_QUADS);
        glNormal3f(1,0,0);
        glVertex3f(x+baseSize, y, z);
        glVertex3f(x+baseSize, y, z+baseSize);
        glVertex3f(x+baseSize, y+height, z+baseSize);
        glVertex3f(x+baseSize, y+height, z);
    glEnd();

    glFlush();
}

void Pino::setPosition(float X, float Y, float Z)
{
    x = X;
    y = Y;
    z = Z;
}

void Pino::goTo(float X, float Z)
{
    x = unid*X+dX;
    z = unid*Z+dZ;
    oldCoordX = coordX;
    oldCoordZ = coordZ;
    coordX = X;
    coordZ = Z;
}

void Pino::andar(int passos)
{
    int k = 0;
    int **matriz = tabuleiro.getMatriz();
    int valorCasa;
    while (k < passos){
        valorCasa = matriz[coordX][coordZ+1];
        if(coordZ < 7 && matriz[coordZ+1][coordX] != 0 && coordZ+1 != oldCoordZ){ //tenta andar pra frente
            z += unid;
            oldCoordX = coordX;
            oldCoordZ = coordZ;
            coordZ++;
        } else if(coordX > 0 && matriz[coordZ][coordX-1] != 0 && coordX-1 != oldCoordX){//tenta andar pra direita
            x -= unid;
            oldCoordX = coordX;
            oldCoordZ = coordZ;
            coordX--;
        } else if(coordX < 7 && matriz[coordZ][coordX+1] != 0 && coordX+1 != oldCoordX){//tenta andar pra esquerda
            x += unid;
            oldCoordX = coordX;
            oldCoordZ = coordZ;
            coordX++;
        } else if(coordZ > 0 && matriz[coordZ-1][coordX] != 0 && coordZ-1 != oldCoordZ){//tenta andar pra tras
            z -= unid;
            oldCoordX = coordX;
            oldCoordZ = coordZ;
            coordZ--;
        }

        k++;
    }
    //tenta andar pra frente
}

void Pino::setFormat(float base, float altura)
{
    baseSize = base;
    height = altura;
}

void Pino::setColor(float R, float G, float B){
    r = R;
    g = G;
    b = B;
}

float Pino::getHeight() const
{
    return height;
}

void Pino::setHeight(float value)
{
    height = value;
}

float Pino::getBaseSize() const
{
    return baseSize;
}

void Pino::setBaseSize(float value)
{
    baseSize = value;
}

float Pino::getScale() const
{
    return scale;
}

void Pino::setScale(float value)
{
    scale = value;
    baseSize *= scale;
    height *= scale;
}

float Pino::getZ() const
{
    return z;
}

void Pino::setZ(float value)
{
    z = value;
}

float Pino::getY() const
{
    return y;
}

void Pino::setY(float value)
{
    y = value;
}

float Pino::getX() const
{
    return x;
}

void Pino::setX(float value)
{
    x = value;
}

float Pino::getUnid() const
{
    return unid;
}

void Pino::setUnid(float value)
{
    unid = value;
}

float Pino::getDX() const
{
    return dX;
}

void Pino::setDX(float value)
{
    dX = value;
    x += dX;
}

float Pino::getDZ() const
{
    return dZ;
}

void Pino::setDZ(float value)
{
    dZ = value;
        z += dZ;
}

int Pino::getCoordZ() const
{
    return coordZ;
}

void Pino::setCoordZ(int value)
{
    coordZ = value;
}

int Pino::getCoordX() const
{
    return coordX;
}

void Pino::setCoordX(int value)
{
    coordX = value;
}

Tabuleiro Pino::getTabuleiro() const
{
    return tabuleiro;
}

void Pino::setTabuleiro(const Tabuleiro &value)
{
    tabuleiro = value;
}

