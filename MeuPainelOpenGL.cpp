﻿#include <QMouseEvent>
#include <QWheelEvent>
#include <QKeyEvent>
#include <QLabel>
#include "MeuPainelOpenGL.h"
#include <cmath>
#include "Util.h"
#include "GL/glu.h"
#include <time.h>
#include <QCoreApplication>

float perto = 40;
float longe = 200;
float lado = 16;


MeuPainelOpenGL::MeuPainelOpenGL(QWidget *parent) :
    QGLWidget(parent)
{    

    setFormat(QGL::DoubleBuffer | QGL::DepthBuffer);
    setFocusPolicy(Qt::StrongFocus);
    malhaSize = 100;

    resetView();

    tabuleiro.setOffset(0.5);
    tabuleiro.setTamanhoCasa(10);
    tabuleiro.createMatriz();

    pino1.setFormat(3,5);
    pino1.setColor(1,1,0);
    pino1.setUnid(tabuleiro.getTamanhoCasa());
    pino1.setPosition(0,tabuleiro.getTamanhoCasa()-tabuleiro.getOffset(),0);
    pino1.setDX(((tabuleiro.getTamanhoCasa()-tabuleiro.getOffset())/2-pino1.getBaseSize())/2);
    pino1.setDZ((tabuleiro.getTamanhoCasa()-tabuleiro.getOffset()-pino1.getBaseSize())/2);
    pino1.setTabuleiro(tabuleiro);

    pino2.setFormat(3,5);
    pino2.setColor(0,1,1);
    pino2.setUnid(tabuleiro.getTamanhoCasa());
    pino2.setPosition(10,tabuleiro.getTamanhoCasa()-tabuleiro.getOffset(),0);
    pino2.setDX((tabuleiro.getTamanhoCasa()-tabuleiro.getOffset())/2+((tabuleiro.getTamanhoCasa()-tabuleiro.getOffset())/2-pino2.getBaseSize())/2);
    pino2.setDZ((tabuleiro.getTamanhoCasa()-tabuleiro.getOffset()-pino2.getBaseSize())/2);
    pino2.setTabuleiro(tabuleiro);

    pino1.goTo(2,0);
    pino2.goTo(2,0);

    jogadorAtual = 0;

    xT = 0;
    yT = 0;
    zT = 0;

    emit hideBotaoDado();

}

void MeuPainelOpenGL::sortear()
{
    srand (time(NULL));

    if(rand() % 2) jogadorAtual = 1;
    else jogadorAtual = -1;

    emit hideBotaoSortear();
    emit showBotaoDado();

    if(jogadorAtual == -1){
        emit setDadoVazio1("");
        emit setJogador2("");
        emit setJogador1("Vez do jogador 1");
    } else{
        emit setJogador2("Vez do jogador 2");
        emit setJogador1("");
        emit setDadoVazio2("");
    }
}

void MeuPainelOpenGL::jogarDado()
{
    int i;
    int wait = 400;
    int dado = rand() % 6 + 1;
    Pino *pinoAtual;
    emit setDadoVazio1("");
    emit setDadoVazio2("");
    if(jogadorAtual == -1){
        emit setValorDado1(dado);
        pinoAtual = &pino2;
    } else{
        emit setValorDado2(dado);
        pinoAtual = &pino1;
    }

    QCoreApplication::instance()->processEvents();
    for(i = 0; i < dado; i++){
        pinoAtual->andar(1);
        updateGL();
        Sleep(wait);
    }

    if(pinoAtual->getCoordX() == 3 && pinoAtual->getCoordZ() == 7){
        if(jogadorAtual == -1){
            emit setJogador1("O jogador 1 foi o vencedor!!!");
            emit setJogador2("");
        } else {
            emit setJogador2("O jogador 2 foi o vencedor!!!");
            emit setJogador1("");
        }
        emit hideBotaoDado();
        emit showBotaoReiniciar();
        return;
    }

    trocarJogador();
}

void MeuPainelOpenGL::reiniciar()
{
    emit hideBotaoReiniciar();
    emit showBotaoSortear();
    emit setDadoVazio1("");
    emit setDadoVazio2("");
    emit setJogador1("");
    emit setJogador2("");
    jogadorAtual = 0;
    pino1.goTo(2,0);
    pino2.goTo(2,0);
}

void MeuPainelOpenGL::trocarJogador(){
    if(jogadorAtual == 1){
        emit setDadoVazio1("");
        emit setJogador1("Vez do jogador 1");
        emit setJogador2("");
    } else{
        emit setJogador1("");
        emit setJogador2("Vez do jogador 2");
        emit setDadoVazio2("");
    }
    jogadorAtual = -jogadorAtual;
}

void MeuPainelOpenGL::resetView(){
    cam.reset();
    xRot = 30*16;
    yRot = 180*16;
    zRot = 0*16;
    zoom = 1;
}

typedef struct vertex_s
{
   float x, y, z;
} vertice;
static int qNormalizeAngle(int angle);

void MeuPainelOpenGL::updateMalha(int valor){
    malhaSize = valor;
    resizeGL(this->width(), this->height());
    updateGL();
}

void MeuPainelOpenGL::initializeGL(){

    glClearColor(.0f, .0f, .0f, .0f);
    glClearDepth(1.0f);

    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_LINE_SMOOTH);

    glDepthFunc(GL_LEQUAL);
    //glShadeModel(GL_SMOOTH);

    glHint(GL_LINE_SMOOTH_HINT, GL_SMOOTH);
    configurarLuz();
}

void MeuPainelOpenGL::resizeGL(int width, int height){

    height = height?height:1;
    windowWidth = (GLfloat)width;
    windowHeight = (GLfloat)height;
    glViewport( 0, 0, windowWidth, windowHeight);

    setupCamera();
}

void MeuPainelOpenGL::setupCamera(){
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(zoom*45,windowWidth/windowHeight,0.1f,malhaSize*3);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(cam.getX(), cam.getY(), cam.getZ(),
    cam.getFoco_x(), cam.getFoco_y(), cam.getFoco_z(),
    0, 1, 0);
    //glGetFloatv(GL_MODELVIEW_MATRIX, cam.matriz);
}

void MeuPainelOpenGL::paintGL() {
    // e Limpa o buffer de teste de profundidade
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPushMatrix();

    glRotatef(xRot/16, 1,0,0);
    glRotatef(yRot/16, 0,1,0);
    glRotatef(zRot/16, 0,0,1);
    glTranslatef(-xT, -yT, -zT);

    //desenhaMalhaChao();
    //desenhaEixos();

    glEnable(GL_LIGHTING);
    glPushMatrix();
        glTranslatef(-tabuleiro.getTamanhoCasa()*8/2, 0, -tabuleiro.getTamanhoCasa()*8/2);
        tabuleiro.desenha();
        pino1.draw();
        pino2.draw();
    glPopMatrix();
    glDisable(GL_LIGHTING);
glPopMatrix();
//    glPopMatrix();

}

void MeuPainelOpenGL::configurarLuz(){
    // Somewhere in the initialization part of your program…
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHT1);

    // Create light components
    GLfloat ambientLight[] = { 0.1f, 0.1f, 0.1f, 1.0f };
    GLfloat diffuseLight0[] = { 0.4f, 0.4f, 0.4, 1.0f };
    GLfloat diffuseLight1[] = { 0.8f, 0.8f, 0.8, 1.0f };
    GLfloat specularLight[] = { 0.5f, 0.0f, 0.0f, 1.0f };
    GLfloat position0[] = { 50, 60, 20, 1.0f };
    GLfloat position1[] = { -50, 50, -50, 1.0f };

    // Assign created components to GL_LIGHT0
    glLightfv(GL_LIGHT0, GL_AMBIENT, ambientLight);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuseLight0);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT0, GL_POSITION, position0);

    glLightfv(GL_LIGHT1, GL_AMBIENT, ambientLight);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuseLight1);
    glLightfv(GL_LIGHT1, GL_SPECULAR, specularLight);
    glLightfv(GL_LIGHT1, GL_POSITION, position1);
    glEnable(GL_COLOR_MATERIAL);
    glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
}
void MeuPainelOpenGL::desenhaMalhaChao(){
    int i;

    glColor3f(0.7,0.7,0.7);
    glBegin(GL_QUADS);
        glNormal3f(0,1,0);
        glVertex3f(-malhaSize, -0.01, malhaSize);
        glVertex3f(-malhaSize, -0.01, -malhaSize);
        glVertex3f(malhaSize, -0.01, -malhaSize);
        glVertex3f(malhaSize, -0.01, malhaSize);
    glEnd();

    glColor3f(0.5,0.5,0.5);
    glLineWidth(0.1);
    glBegin(GL_LINES);
    for(i = -malhaSize; i < malhaSize; i+=1){
        glVertex3f(-malhaSize, -0.005, i);
        glVertex3f(malhaSize, -0.005, i);
        glVertex3f(i, -0.005, malhaSize);
        glVertex3f(i, -0.005, -malhaSize);
    }
    glEnd();

}

void MeuPainelOpenGL::desenhaEixos(){
    glLineWidth(1);
    glBegin(GL_LINES);

        // Eixo X
        glColor3f(1.0f,0.0f,0.0f);
        glVertex3f(0, -0.01, 0);
        glVertex3f(malhaSize, -0.01, 0);
        // Eixo X NEGATIVO
        glColor3f(1.0f,0.8f,0.8f);
        glVertex3f(0, -0.01, 0);
        glVertex3f(-malhaSize, -0.01, 0);

        // Eixo Y
        glColor3f(0.0f,0.0f,1.0f);
        glVertex3f(0, 0, 0);
        glVertex3f(0, malhaSize, 0);
        // Eixo Y NEGATIVO
        glColor3f(0.8f,0.8f,1.0f);
        glVertex3f(0, 0, 0);
        glVertex3f(0, -malhaSize, 0);

        // Eixo Z
        glColor3f(0.0f,1.0f,0.0f);
        glVertex3f(0, -0.01, 0);
        glVertex3f(0, -0.01, malhaSize);
        // Eixo Z NEGATIVO
        glColor3f(0.8f,1.0f,0.8f);
        glVertex3f(0, -0.01, 0);
        glVertex3f(0, -0.01, -malhaSize);

    glEnd();
}

void MeuPainelOpenGL::mousePressEvent(QMouseEvent *event){
    lastPoint = event->pos();
 }

void MeuPainelOpenGL::mouseMoveEvent(QMouseEvent *event)
{
    float dx = event->x() - lastPoint.x();
    float dy = event->y() - lastPoint.y();

    if (event->buttons() & Qt::LeftButton) {
        setYRotation(qNormalizeAngle(yRot + dx*8));
        setXRotation(qNormalizeAngle(xRot + dy*8));
        updateGL();
    }
    lastPoint = event->pos();
}

void MeuPainelOpenGL::wheelEvent(QWheelEvent *event){
    int delta = event->delta();
    if(delta > 0){ //zoom in
        setZoom(getZoom()-0.1);
    } else{ //zoom out
        setZoom(getZoom()+0.1);
    }
    qDebug("%f", zoom);

    setupCamera();
    updateGL();
}

void MeuPainelOpenGL::keyPressEvent(QKeyEvent *e){

    switch (e->key())
    {
    case Qt::Key_Escape:
    case Qt::Key_Q:
        parentWidget()->close();
        exit(0);
        break;
    case Qt::Key_T:
        // tela transformações
        break;
    case Qt::Key_R:
        resetView();
        setupCamera();
        updateGL();
        //this->malha = false;
        break;
    case Qt::Key_P:

        break;
    case Qt::Key_Up:
        xRot++;
        break;
    case Qt::Key_Down:
        xRot--;
        break;
    case Qt::Key_Left:
        yRot--;
        break;
    case Qt::Key_Right:
        yRot++;
        break;
    case Qt::Key_0:
        glEnable(GL_LIGHT0);
        break;
    case Qt::Key_1:
        perto--;
        //cam.rotateX(1);
        glDisable(GL_LIGHT0);
        setupCamera();
        break;
    case Qt::Key_2:
        perto++;
        //cam.rotateX(-1);
        glEnable(GL_LIGHT1);
        setupCamera();
        break;
    case Qt::Key_3:
        glDisable(GL_LIGHT1);
        break;
    case Qt::Key_4:
        longe--;
        setupCamera();
        break;
    case Qt::Key_5:
        longe++;
        setupCamera();
        break;
    case Qt::Key_7:
        lado-=0.1;
        setupCamera();
        break;
    case Qt::Key_8:
        lado+=0.1;
        setupCamera();
        break;
    case Qt::Key_W:
        //cam.moveForward(1);
        break;
    case Qt::Key_S:
        //cam.moveBackward(1);
        break;
    case Qt::Key_A:
        pino1.andar(1);
        //cam.moveLeft(1);
        break;
    case Qt::Key_D:
        pino2.andar(1);
        //cam.moveRight(1);
        break;


    }
    //qDebug("Perto: %f\nLonge: %f\nSize: %f", perto, longe, lado);
    qDebug("%f %f %f",cam.getX(), cam.getY(), cam.getZ());

    updateGL();
}

void MeuPainelOpenGL::setXRotation(int angle)
{

    if(angle > 0 && angle < 1141){
        if (angle != xRot) {
            xRot = angle;
            qDebug("%i", xRot);
        }
    }
}

void MeuPainelOpenGL::setYRotation(int angle)
{
    if (angle != yRot) {
        yRot = angle;
    }
}

void MeuPainelOpenGL::setZRotation(int angle)
{
    if (angle != zRot) {
        zRot = angle;
    }
}

void MeuPainelOpenGL::setZoom(float valor){
    if(valor > 0 && valor < 2){
        zoom = valor;
        zT = zoom;
    }
}

float MeuPainelOpenGL::getZoom(){
    return this->zoom;
}

static int qNormalizeAngle(int angle)
{
    while (angle < 0)
        angle += 360 * 16;
    while (angle > 360 * 16)
        angle -= 360 * 16;
    return angle;
}
