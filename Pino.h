#ifndef PINO_H
#define PINO_H

#include "Tabuleiro.h"



class Pino
{
public:
    Pino();

    void draw();

    void setColor(float R, float G, float B);

    void setPosition(float X, float Y, float Z);

    void goTo(float X, float Y);

    void andar(int passos);

    void setFormat(float base, float altura);

    float getHeight() const;
    void setHeight(float value);

    float getBaseSize() const;
    void setBaseSize(float value);

    float getScale() const;
    void setScale(float value);

    float getZ() const;
    void setZ(float value);

    float getY() const;
    void setY(float value);

    float getX() const;
    void setX(float value);

    float getUnid() const;
    void setUnid(float value);

    float getDX() const;
    void setDX(float value);

    float getDZ() const;
    void setDZ(float value);

    int getCoordZ() const;
    void setCoordZ(int value);

    int getCoordX() const;
    void setCoordX(int value);

    Tabuleiro getTabuleiro() const;
    void setTabuleiro(const Tabuleiro &value);

private:
    Tabuleiro tabuleiro;
    int coordX, coordZ, oldCoordX, oldCoordZ;
    float unid;
    float x, y, z, dX, dZ;
    float r, g, b;
    float scale;
    float baseSize;
    float height;
};

#endif // PINO_H
