#include "Camera.h"
#include <GL/glu.h>
#include <math.h>

#define toRad(angleDegrees) (angleDegrees * M_PI / 180.0)


Camera::Camera(){

}
void Camera::reset(){
    x = 00;
    y = 0.1;
    z = 100;
    foco_x = 0;
    foco_y = 0;
    foco_z = 0;
}

void Camera::setAll(float x, float y, float z, float foco_x, float foco_y, float foco_z){
    this->x = x;
    this->y = y;
    this->z = z;
    this->foco_x = foco_x;
    this->foco_y = foco_y;
    this->foco_z = foco_z;
}

void Camera::rotateX(float a)
{
    a = toRad(a);
    //glRotatef(a,1,0,0);
//    setY(y_*cos(a)-z_*sin(a));
//    setZ(y_*sin(a)+z_*cos(a));

    // a rotação no X na verdade é uma rotação vertical em torno do ponto focal

    float x_ = x;
    float y_ = y;
    float z_ = z;
    float fx = foco_x;
    float fy = foco_y;
    float fz = foco_z;
    float h = sqrt(pow(x_,2)+pow(z_,2));
    float senA = x_/h;
    float cosA = z_/h;
    x = -cosA*fx+fx+cosA*x_-fz*senA*cos(a)+senA*z_*cos(a)-fy*senA*sin(a)+senA*y_*sin(a);
    y = fy-fy*cos(a)+y_*cos(a)+fz*sin(a)-z_*sin(a);
    z = fz+senA*fx-senA*x_-fz*cosA*cos(a)+cosA*z_*cos(a)-fy*cosA*sin(a)+cosA*y_*sin(a);
}

void Camera::rotateY(float a)
{
    a = toRad(a);
   // rotacao em torno do foco
    float x_ = x;
    float z_ = z;
  setX((x_*cos(a)+z_*sin(a)));
  setZ((-x_*sin(a)+z_*cos(a)));

    //rotacao no em torno da camera:
//    float fx = foco_x;
//    float fz = foco_z;
//    foco_x = x+fx*cos(a)-x*cos(a)+fz*sin(a)-z*sin(a);
//    foco_z = z+fz*cos(a)-z*cos(a)-fx*sin(a)+x*sin(a);
}

void Camera::rotateZ(float a)
{
    a = toRad(a);
    float x_ = x;
    float y_ = y;
    //glRotatef(angulo,0,0,1);
    setX(x_*cos(a)-y_*sin(a));
    setY(x_*sin(a)+y_*cos(a));
}

void Camera::rotate(float x, float y, float z)
{
    rotateX(x);
    rotateY(y);
    rotateZ(z);
}


void Camera::desenhaFoco()
{
    glColor3f(1,0,0);
    glBegin(GL_POINTS);
    glVertex3f(foco_x, foco_y, foco_z);
    glEnd();
}

float Camera::getFoco_x() const
{
    return foco_x;
}

void Camera::setFoco_x(float value)
{
    foco_x = value;
}

float Camera::getY() const
{
    return y;
}

void Camera::setY(float value)
{
    y = value;
}

float Camera::getZ() const
{
    return z;
}

void Camera::setZ(float value)
{
    z = value;
}

float Camera::getX() const
{
    return x;
}

void Camera::setX(float value)
{
    x = value;
}

float Camera::getFoco_y() const
{
    return foco_y;
}

void Camera::setFoco_y(float value)
{
    foco_y = value;
}

float Camera::getFoco_z() const
{
    return foco_z;
}

void Camera::setFoco_z(float value)
{
    foco_z = value;
}

void Camera::moveForward(float distance){
    float speed = distance/15;
    float dx = foco_x - x;
    float dy = foco_y - y;
    float dz = foco_z - z;
    x += dx * speed;
    y += dy * speed;
    z += dz * speed;
    foco_x += dx * speed;
    foco_y += dy * speed;
    foco_z += dz * speed;
}

void Camera::moveBackward(float distance){
    float speed = distance/15;
    float dx = foco_x - x;
    float dy = foco_y - y;
    float dz = foco_z - z;
    x -= dx * speed;
    y -= dy * speed;
    z -= dz * speed;
    foco_x -= dx * speed;
    foco_y -= dy * speed;
    foco_z -= dz * speed;
}


void Camera::moveLeft(float distance){
    float speed = distance/15;
    float dx = foco_x - x;
    float dy = foco_y - y;
    float dz = foco_z - z;

    dx = 1 * dz - dy * 0;
    dy = 0 * dx - dz * 0;
    dz = 0 * dy - dx * 1;

    x += dx * speed;
    y += dy * speed;
    z += dz * speed;
    foco_x += dx * speed;
    foco_y += dy * speed;
    foco_z += dz * speed;
}

void Camera::moveRight(float distance){
    float speed = distance/15;
    float dx = foco_x - x;
    float dy = foco_y - y;
    float dz = foco_z - z;

    dx = 1 * dz - dy * 0;
    dy = 0 * dx - dz * 0;
    dz = 0 * dy - dx * 1;

    dx = dy * 0 - 1 * dz;
    dy = dz * 0 - 0 * dx;
    dz = dx * 1 - 0 * dy;

    x -= dx * speed;
    y -= dy * speed;
    z -= dz * speed;
    foco_x -= dx * speed;
    foco_y -= dy * speed;
    foco_z -= dz * speed;
}
